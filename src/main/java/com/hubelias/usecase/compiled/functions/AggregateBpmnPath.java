package com.hubelias.usecase.compiled.functions;

import com.hubelias.usecase.compiled.CompiledUseCase;
import com.hubelias.usecase.compiled.QueryableGraph;

public class AggregateBpmnPath implements QueryableGraph.AggregationFunction<String, CompiledUseCase.CompiledBranch, AggregateBpmnPath> {
    private final String bpmnPath;

    public AggregateBpmnPath() {
        this(null);
    }

    private AggregateBpmnPath(String bpmnPath) {
        this.bpmnPath = bpmnPath;
    }

    @Override
    public AggregateBpmnPath append(QueryableGraph.Vertex<CompiledUseCase.CompiledBranch> vertex) {
        return bpmnPath == null
                ? new AggregateBpmnPath(vertex.content.bpmnSeq)
                : new AggregateBpmnPath(bpmnPath + "," + vertex.content.bpmnSeq);
    }

    @Override
    public AggregateBpmnPath prepend(QueryableGraph.Vertex<CompiledUseCase.CompiledBranch> vertex) {
        return this;
    }

    @Override
    public String getValue() {
        return bpmnPath;
    }
}
