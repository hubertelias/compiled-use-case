package com.hubelias.usecase.compiled.functions;

import com.hubelias.usecase.compiled.CompiledUseCase;
import com.hubelias.usecase.compiled.QueryableGraph;
import io.vavr.collection.HashSet;

public class CollectTags implements QueryableGraph.AggregationFunction<HashSet<String>, CompiledUseCase.CompiledBranch, CollectTags> {

    private final HashSet<String> tags;

    public CollectTags() {
        this(HashSet.empty());
    }

    private CollectTags(HashSet<String> tags) {
        this.tags = tags;
    }

    @Override
    public CollectTags append(QueryableGraph.Vertex<CompiledUseCase.CompiledBranch> vertex) {
        return new CollectTags(tags.addAll(vertex.content.tags));
    }

    @Override
    public CollectTags prepend(QueryableGraph.Vertex<CompiledUseCase.CompiledBranch> vertex) {
        return this;
    }

    @Override
    public HashSet<String> getValue() {
        return tags;
    }
}
