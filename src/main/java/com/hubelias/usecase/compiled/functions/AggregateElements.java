package com.hubelias.usecase.compiled.functions;

import com.hubelias.usecase.compiled.CompiledUseCase;
import com.hubelias.usecase.compiled.QueryableGraph;

public class AggregateElements implements QueryableGraph.AggregationFunction<Integer, CompiledUseCase.CompiledBranch, AggregateElements> {
    private final int globalDefaults;
    private final int backwardsAggregation;

    public AggregateElements(int globalDefaults) {
        this(globalDefaults, 0);
    }

    private AggregateElements(int globalDefaults, int backwardsAggregation) {
        this.globalDefaults = globalDefaults;
        this.backwardsAggregation = backwardsAggregation;
    }

    @Override
    public AggregateElements append(QueryableGraph.Vertex<CompiledUseCase.CompiledBranch> vertex) {
        return this;
    }

    @Override
    public AggregateElements prepend(QueryableGraph.Vertex<CompiledUseCase.CompiledBranch> vertex) {
        return new AggregateElements(globalDefaults, backwardsAggregation + vertex.content.someDataToAggregate);
    }

    @Override
    public Integer getValue() {
        // prepend with global defaults
        return globalDefaults + backwardsAggregation;
    }
}
