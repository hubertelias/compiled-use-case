package com.hubelias.usecase.compiled;

import io.vavr.collection.*;
import io.vavr.control.Either;

import java.util.function.*;

public abstract class QueryableGraph<CONTENT> {
    private final Vertex<CONTENT> root;

    QueryableGraph(Vertex<CONTENT> root) {
        this.root = root;
    }

    public <OUT> Seq<OUT> executeQuery(Consumer<Query<CONTENT, OUT>> query) {
        return root.executeQuery(query, GoForward.init()).flatMap(BackwardQuery::emit);
    }

    public interface Query<CONTENT, OUT> {
        <A> A aggregate(
                Supplier<A> initialValue,
                BiFunction<A, Vertex<CONTENT>, A> append,
                BiFunction<A, Vertex<CONTENT>, A> prepend
        );

        default <A> A aggregateForward(
                Supplier<A> initialValue,
                BiFunction<A, Vertex<CONTENT>, A> append
        ) {
            return aggregate(initialValue, append, (prev, vertex) -> prev);
        }

        default <A> A aggregateBackward(
                Supplier<A> initialValue,
                BiFunction<A, Vertex<CONTENT>, A> prepend
        ) {
            return aggregate(initialValue, (prev, vertex) -> prev, prepend);
        }

        default <A, AF extends AggregationFunction<A, CONTENT, AF>> A aggregate(Supplier<AF> initialValue) {
            return aggregate(initialValue, AggregationFunction::append, AggregationFunction::prepend).getValue();
        }

        void filter(Predicate<Vertex<CONTENT>> shouldContinueForward, Predicate<Vertex<CONTENT>> shouldContinueBackward, Function<Vertex<CONTENT>, OUT> elseEmit);

        default void filterForward(Predicate<Vertex<CONTENT>> shouldContinue) {
            filter(shouldContinue, (vertex) -> true, null);
        }

        default void filterForward(Predicate<Vertex<CONTENT>> shouldContinue, Function<Vertex<CONTENT>, OUT> elseEmit) {
            filter(shouldContinue, (vertex) -> true, elseEmit);
        }

        default void filterBackward(Predicate<Vertex<CONTENT>> shouldContinue) {
            filter((vertex) -> true, shouldContinue, null);
        }

        default void filterBackward(Predicate<Vertex<CONTENT>> shouldContinue, Function<Vertex<CONTENT>, OUT> elseEmit) {
            filter((vertex) -> true, shouldContinue, elseEmit);
        }

        void emitBackwards(Supplier<OUT> value);
    }

    public interface AggregationFunction<A, CONTENT, SELF extends AggregationFunction<A, CONTENT, SELF>> {
        SELF append(Vertex<CONTENT> vertex);

        SELF prepend(Vertex<CONTENT> vertex);

        A getValue();
    }

    private static class GoForward<CONTENT, OUT> implements Query<CONTENT, OUT> {
        private final Vertex<CONTENT> visitedVertex;
        private Array<Object> data;
        private int aggregationIndex;
        private Seq<Supplier<OUT>> emitted = List.empty();

        private static <CONTENT, OUT> GoForward<CONTENT, OUT> init() {
            return new GoForward<>(null, Array.empty());
        }

        private GoForward(Vertex<CONTENT> visitedVertex, Array<Object> data) {
            this.visitedVertex = visitedVertex;
            this.data = data;
        }

        @SuppressWarnings("unchecked")
        @Override
        public <A> A aggregate(Supplier<A> initialValue, BiFunction<A, Vertex<CONTENT>, A> append, BiFunction<A, Vertex<CONTENT>, A> prepend) {
            if (data.size() <= aggregationIndex) {
                data = data.append(initialValue.get());
            }
            A newValue = (A) append.apply((A) data.get(aggregationIndex), visitedVertex);
            data = data.update(aggregationIndex, newValue);
            aggregationIndex++;
            return newValue;
        }

        @Override
        public void filter(Predicate<Vertex<CONTENT>> shouldContinueForward, Predicate<Vertex<CONTENT>> shouldContinueBackward, Function<Vertex<CONTENT>, OUT> elseEmit) {
            if (!shouldContinueForward.test(visitedVertex)) {
                if(elseEmit != null) {
                    emitted = emitted.append(() -> elseEmit.apply(visitedVertex));
                }
                throw new FilteredException();
            }
        }

        @Override
        public void emitBackwards(Supplier<OUT> value) {
        }

        Either<FilteredOut<CONTENT, OUT>, GoForward<CONTENT, OUT>> append(Consumer<Query<CONTENT, OUT>> query, Vertex<CONTENT> vertex) {
            GoForward<CONTENT, OUT> next = new GoForward<>(vertex, data);
            try {
                query.accept(next);
            } catch (FilteredException e) {
                return Either.left(new FilteredOut<>(next.emitted));
            }
            return Either.right(next);
        }

        GoBackward<CONTENT, OUT> goBackward() {
            return new GoBackward<>(null, data);
        }
    }

    private interface BackwardQuery<CONTENT, OUT> extends Query<CONTENT, OUT> {
        BackwardQuery<CONTENT, OUT> prepend(Consumer<Query<CONTENT, OUT>> query, Vertex<CONTENT> vertex);

        Stream<OUT> emit();
    }

    private static class GoBackward<CONTENT, OUT> implements BackwardQuery<CONTENT, OUT> {
        private final Vertex<CONTENT> visitedVertex;
        private Array<Object> data;
        private int aggregationIndex;
        private Seq<Supplier<OUT>> emitted = List.empty();

        private GoBackward(Vertex<CONTENT> visitedVertex, Array<Object> data) {
            this.visitedVertex = visitedVertex;
            this.data = data;
        }

        @Override
        @SuppressWarnings("unchecked")
        public <A> A aggregate(Supplier<A> initialValue, BiFunction<A, Vertex<CONTENT>, A> append, BiFunction<A, Vertex<CONTENT>, A> prepend) {
            A afterPrepend = prepend.apply((A) data.get(aggregationIndex), visitedVertex);
            data = data.update(aggregationIndex, afterPrepend);
            aggregationIndex++;
            return afterPrepend;
        }

        @Override
        public void filter(Predicate<Vertex<CONTENT>> shouldContinueForward, Predicate<Vertex<CONTENT>> shouldContinueBackward, Function<Vertex<CONTENT>, OUT> elseEmit) {
            if (!shouldContinueBackward.test(visitedVertex)) {
                if (elseEmit != null) {
                    emitted = emitted.append(() -> elseEmit.apply(visitedVertex));
                }
                throw new FilteredException();
            }
        }

        @Override
        public void emitBackwards(Supplier<OUT> value) {
            emitted = emitted.append(value);
        }

        @Override
        public BackwardQuery<CONTENT, OUT> prepend(Consumer<Query<CONTENT, OUT>> query, Vertex<CONTENT> vertex) {
            GoBackward<CONTENT, OUT> next = new GoBackward<>(vertex, data);
            try {
                query.accept(next);
            } catch (FilteredException e) {
                return new FilteredOut<>(next.emitted);
            }
            return next;
        }

        @Override
        public Stream<OUT> emit() {
            return emitted.toStream().map(Supplier::get);
        }
    }

    private static class FilteredException extends RuntimeException {
    }

    private static class FilteredOut<CONTENT, OUT> implements BackwardQuery<CONTENT, OUT> {
        private final Seq<Supplier<OUT>> emitted;

        FilteredOut(Seq<Supplier<OUT>> emitted) {
            this.emitted = emitted;
        }

        @Override
        public <A> A aggregate(Supplier<A> initialValue, BiFunction<A, Vertex<CONTENT>, A> append, BiFunction<A, Vertex<CONTENT>, A> prepend) {
            throw new UnsupportedOperationException();
        }

        @Override
        public void filter(Predicate<Vertex<CONTENT>> shouldContinueForward, Predicate<Vertex<CONTENT>> shouldContinueBackward, Function<Vertex<CONTENT>, OUT> elseEmit) {
            throw new UnsupportedOperationException();
        }

        @Override
        public void emitBackwards(Supplier<OUT> value) {
            throw new UnsupportedOperationException();
        }

        @Override
        public BackwardQuery<CONTENT, OUT> prepend(Consumer<Query<CONTENT, OUT>> query, Vertex<CONTENT> vertex) {
            return this;
        }

        @Override
        public Stream<OUT> emit() {
            return emitted.toStream().map(Supplier::get);
        }
    }

    public static class Vertex<CONTENT> {
        public final CONTENT content;
        public final int outgoingPaths;
        private final boolean terminal;
        private final Set<Vertex<CONTENT>> next;

        public Vertex(CONTENT content, boolean terminal, Set<Vertex<CONTENT>> next) {
            this.content = content;
            this.outgoingPaths = 1;
            this.terminal = terminal;
            this.next = next;
        }

        <OUT> Seq<BackwardQuery<CONTENT, OUT>> executeQuery(Consumer<Query<CONTENT, OUT>> query, GoForward<CONTENT, OUT> prev) {
            Either<FilteredOut<CONTENT, OUT>, GoForward<CONTENT, OUT>> current = prev.append(query, this);

            if (current.isLeft()) {
                return Stream.of(current.getLeft());
            }
            GoForward<CONTENT, OUT> prefix = current.get();

            if (terminal) {
                return Stream.of(prefix.goBackward().prepend(query, this));
            }

            return next.toStream()
                    .flatMap(vertex -> vertex.executeQuery(query, prefix))
                    .map(x -> x.prepend(query, this));
        }
    }
}
