package com.hubelias.usecase.compiled;

import io.vavr.collection.HashSet;

public class CompiledUseCase extends QueryableGraph<CompiledUseCase.CompiledBranch> {
    CompiledUseCase(Vertex<CompiledBranch> root) {
        super(root);
    }

    public static class CompiledBranch {
        public final HashSet<String> tags;
        public final int someDataToAggregate;
        public final String bpmnSeq;

        public CompiledBranch(HashSet<String> tags, int someDataToAggregate, String bpmnSeq) {
            this.tags = tags;
            this.someDataToAggregate = someDataToAggregate;
            this.bpmnSeq = bpmnSeq;
        }
    }
}
