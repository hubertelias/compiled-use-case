package com.hubelias.usecase.compiled;

import com.hubelias.usecase.compiled.CompiledUseCase.CompiledBranch;
import com.hubelias.usecase.compiled.QueryableGraph.Vertex;
import com.hubelias.usecase.compiled.functions.AggregateBpmnPath;
import com.hubelias.usecase.compiled.functions.AggregateElements;
import com.hubelias.usecase.compiled.functions.CollectTags;
import io.vavr.Tuple;
import io.vavr.Tuple2;
import io.vavr.collection.HashSet;
import io.vavr.collection.Seq;
import io.vavr.control.Either;
import org.junit.Test;

import static org.assertj.core.api.Assertions.assertThat;

public class CompiledUseCaseTest {

    @Test
    public void shouldAggregateTagsForward() {
        Seq<Object> tagsStream = simpleOne().executeQuery(query -> {
            HashSet<Object> tags = query.aggregateForward(HashSet::empty, (agg, branch) -> agg.addAll(branch.content.tags));
            query.emitBackwards(() -> tags);
        });

        assertThat(tagsStream).containsExactlyInAnyOrder(
                HashSet.of("tag", "tag2"), HashSet.of("tag", "tag3"));
    }

    @Test
    public void shouldAggregateTagsBackward() {
        Seq<HashSet<String>> tagsStream = simpleOne().executeQuery(query -> {
            HashSet<String> tags = query.aggregateBackward(HashSet::empty, (agg, branch) -> agg.addAll(branch.content.tags));
            query.emitBackwards(() -> tags);
        });

        assertThat(tagsStream).containsExactlyInAnyOrder(
                HashSet.of("tag", "tag2"), HashSet.of("tag", "tag3"));
    }

    @Test
    public void shouldEmitAllSequences() {
        Seq<String> bpmnSequences = graphWithJoin().executeQuery(query -> {
            String seq = query.aggregateForward(() -> "", (agg, branch) -> agg.isEmpty()
                    ? branch.content.bpmnSeq
                    : agg + "," + branch.content.bpmnSeq);
            query.emitBackwards(() -> seq);
        });

        assertThat(bpmnSequences).containsExactlyInAnyOrder(
                "S,A1,X,A2,E", "S,A1,X,B2,E", "S,B1,X,A2,E", "S,B1,X,B2,E"
        );
    }

    @Test
    public void shouldAggregatePathWithEncapsulatedFunction() {
        Seq<String> bpmnSequences = graphWithJoin().executeQuery(query -> {
            String seq = query.aggregate(AggregateBpmnPath::new);
            query.emitBackwards(() -> seq);
        });

        assertThat(bpmnSequences).containsExactlyInAnyOrder(
                "S,A1,X,A2,E", "S,A1,X,B2,E", "S,B1,X,A2,E", "S,B1,X,B2,E"
        );
    }

    @Test
    public void shouldAllowForComposingAggregations() {
        Seq<Tuple2<String, Integer>> bpmnSequences = graphWithJoin().executeQuery(query -> {
            String seq = query.aggregate(AggregateBpmnPath::new);
            Integer aggregatedElements = query.aggregate(() -> new AggregateElements(10));
            query.emitBackwards(() -> Tuple.of(seq, aggregatedElements));
        });

        assertThat(bpmnSequences).containsExactlyInAnyOrder(
                Tuple.of("S,A1,X,A2,E", 28),
                Tuple.of("S,A1,X,B2,E", 33),
                Tuple.of("S,B1,X,A2,E", 25),
                Tuple.of("S,B1,X,B2,E", 30)
        );
    }

    @Test
    public void shouldAllowForFilteringTagsForward() {
        Seq<String> filteredPaths = graphWithJoin().executeQuery(query -> {
            String bpmnSeq = query.aggregate(AggregateBpmnPath::new);
            HashSet<String> tags = query.aggregate(CollectTags::new);
            query.filterForward((v) -> !tags.contains("EXCLUDE-ME"));
            query.emitBackwards(() -> bpmnSeq);
        });

        assertThat(filteredPaths).containsExactlyInAnyOrder(
                "S,A1,X,A2,E", "S,B1,X,A2,E"
        );
    }

    @Test
    public void shouldAllowForFilteringTagsBackward() {
        Seq<String> filteredPaths = graphWithJoin().executeQuery(query -> {
            String bpmnSeq = query.aggregate(AggregateBpmnPath::new);
            HashSet<String> tags = query.aggregate(CollectTags::new);
            query.filterBackward((v) -> tags.contains("INCLUDE-ME"));
            query.emitBackwards(() -> bpmnSeq);
        });

        assertThat(filteredPaths).containsExactlyInAnyOrder(
                "S,B1,X,A2,E", "S,B1,X,B2,E"
        );
    }

    @Test
    public void shouldAllowForFilteringTagsInBothDirections() {
        Seq<String> filteredPaths = graphWithJoin().executeQuery(query -> {
            String bpmnSeq = query.aggregate(AggregateBpmnPath::new);
            HashSet<String> tags = query.aggregate(CollectTags::new);
            query.filterForward((v) -> !tags.contains("EXCLUDE-ME"));
            query.filterBackward((v) -> tags.contains("INCLUDE-ME"));
            query.emitBackwards(() -> bpmnSeq);
        });

        assertThat(filteredPaths).containsExactlyInAnyOrder(
                "S,B1,X,A2,E"
        );
    }

    @Test
    public void shouldAllowForEmittingValueFromFilteredPath() {
        Seq<Either<String, String>> filteredPaths = graphWithJoin().executeQuery(query -> {
            String bpmnSeq = query.aggregate(AggregateBpmnPath::new);
            HashSet<String> tags = query.aggregate(CollectTags::new);
            query.filterForward((v) -> !tags.contains("EXCLUDE-ME"), (v) -> Either.left(bpmnSeq));
            query.filterBackward((v) -> tags.contains("INCLUDE-ME"), (v) -> Either.left(bpmnSeq));
            query.emitBackwards(() -> Either.right(bpmnSeq));
        });

        assertThat(filteredPaths).containsExactlyInAnyOrder(
                Either.left("S,A1,X,A2,E"),
                Either.right("S,B1,X,A2,E"),
                Either.left("S,A1,X,B2"),
                Either.left("S,B1,X,B2")
        );
    }

    private CompiledUseCase simpleOne() {
        return new CompiledUseCase(new Vertex<>(new CompiledBranch(HashSet.of("tag"), 1, "S"), false, HashSet.of(
                new Vertex<>(new CompiledBranch(HashSet.of("tag2"), 2, "A1"), true, HashSet.empty()),
                new Vertex<>(new CompiledBranch(HashSet.of("tag3"), -1, "B1"), true, HashSet.empty())
        )));
    }

    private CompiledUseCase graphWithJoin() {
        Vertex<CompiledBranch> end = new Vertex<>(new CompiledBranch(HashSet.empty(), 10, "E"), true, HashSet.empty());

        Vertex<CompiledBranch> join = new Vertex<>(new CompiledBranch(HashSet.of("X-tag"), 2, "X"), false, HashSet.of(
                new Vertex<>(new CompiledBranch(HashSet.of("A-tag", "2-tag"), 3, "A2"), false, HashSet.of(end)),
                new Vertex<>(new CompiledBranch(HashSet.of("2-tag", "EXCLUDE-ME"), 8, "B2"), false, HashSet.of(end))
        ));

        return new CompiledUseCase(new Vertex<>(new CompiledBranch(HashSet.empty(), 1, "S"), false, HashSet.of(
                new Vertex<>(new CompiledBranch(HashSet.of("A-tag", "1-tag"), 2, "A1"), false, HashSet.of(join)),
                new Vertex<>(new CompiledBranch(HashSet.of("1-tag", "INCLUDE-ME"), -1, "B1"), false, HashSet.of(join)))
        ));
    }
}